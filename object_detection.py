from TwitterAPI import TwitterAPI;
import RPi.GPIO as GPIO;
import numpy as np;
import cv2;
import sys;
import time;

#--- CONFIG
RPI_LED_OUT=22;
OUTPUT_FILE_FORMAT='.jpg';
TWITTER_CONSUMER_KEY='2Ay9TRaV7YQDGlcASCuqsgi2W';
TWITTER_CONSUMER_SECRET='Rqx6zKP7Pf0d2s3CUgxDsZCsfS4r7NNyTgPdu6BgT87YQeDoZw';
TWITTER_TOKEN_KEY='3304707714-paMM8UrvOw8OSV8VOcTi1a6TnHcgd6CUPAJQ4zE';
TWITTER_TOKEN_SECRET='057HQVZxDgcbZEZCmO3G2YphWqWRx932399CtTYgIhkXJ';
#--- CONFIG

if len(sys.argv)>1:
    img=cv2.imread(sys.argv[1]);
else:
    img=cv2.imread('test2.jpg');

face_cascade=cv2.CascadeClassifier('detectors/haarcascade_frontalface_default.xml');
eye_cascade=cv2.CascadeClassifier('detectors/haarcascade_eye.xml');
gray=cv2.cvtColor(img, cv2.COLOR_BGR2GRAY);

faces=face_cascade.detectMultiScale(gray, 1.3, 5);
#Returns the number of faces detected in the image
count=len(faces);
#Loop through the faces detected and put on a border
for (x,y,w,h) in faces:
    img=cv2.rectangle(img, (x,y), (x+w,y+h), (255,0,0), 2)
    roi_gray=gray[y:y+h, x:x+w];
    roi_color=img[y:y+h, x:x+w];
    eyes=eye_cascade.detectMultiScale(roi_gray)
    for (ex, ey, ew, eh) in eyes:
        cv2.rectangle(roi_color, (ex, ey), (ex+ew, ey+eh), (0,255,0), 2);
    #--- To display on window the detection results
    #cv2.imshow('img', img);
    #cv2.waitKey(0);
    #cv2.destroyAllWindows();
    #--- To display on window the detection results

#--- To save in a file the results
if count>0:
    filename='{}{}'.format(int(time.time()), OUTPUT_FILE_FORMAT);
    cv2.imwrite(filename, img);
    #--- Posting via Twitter
    api = TwitterAPI(TWITTER_CONSUMER_KEY, TWITTER_CONSUMER_SECRET, TWITTER_TOKEN_KEY, TWITTER_TOKEN_SECRET);
    file = open(filename, 'rb');
    data = file.read()
    r = api.request('statuses/update_with_media', {'status':'People detected'}, {'media[]':data})
    if r.status_code==200:
        GPIO.setmode(GPIO.BCM);
        GPIO.setwarnings(False);
        GPIO.setup(RPI_LED_OUT, GPIO.OUT);
        print("Success: LED on");
        GPIO.output(RPI_LED_OUT, GPIO.HIGH);
        time.sleep(1);
        print("Success: LED off");
        GPIO.output(RPI_LED_OUT, GPIO.LOW);
    #--- Posting via Twitter
#--- To save in a file the results

#--- SAMPLE USAGE
#python object_detection.py test.jpg
#--- SAMPLE USAGE
